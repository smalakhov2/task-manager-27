package ru.malakhov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_session")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionDto extends AbstractEntityDto implements Cloneable {

    public static final long serialVersionUID = 1L;

    @Nullable
    @Column(name = "timestamp")
    private Long timestamp;

    @Nullable
    @Column(name = "user_id")
    private String userId;

    @Nullable
    @Column(name = "signature")
    private String signature;

    public SessionDto(
            @NotNull final Long timestamp,
            @NotNull final String userId
    ) {
        this.timestamp = timestamp;
        this.userId = userId;
    }

    public SessionDto(
            @NotNull final Long timestamp,
            @NotNull final String userId,
            @Nullable final String signature
    ) {
        this.timestamp = timestamp;
        this.userId = userId;
        this.signature = signature;
    }

    @Nullable
    @Override
    public SessionDto clone() {
        try {
            return (SessionDto) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o instanceof SessionDto && super.equals(o)) {
            @NotNull final SessionDto session = (SessionDto) o;
            return Objects.equals(timestamp, session.timestamp)
                    && Objects.equals(userId, session.userId)
                    && Objects.equals(signature, session.signature);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, timestamp, userId, signature);
    }

    @Nullable
    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(@NotNull Long timestamp) {
        this.timestamp = timestamp;
    }

    @Nullable
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NotNull String userId) {
        this.userId = userId;
    }

    @Nullable
    public String getSignature() {
        return signature;
    }

    public void setSignature(@Nullable String signature) {
        this.signature = signature;
    }

}