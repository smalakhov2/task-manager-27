package ru.malakhov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_project")
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Project extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @Nullable
    private String description = "";

    @NotNull
    @ManyToOne
    private User user;

    @NotNull
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    public Project(@NotNull final String name) {
        this.name = name;
    }

    public Project(@NotNull final String name, @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

    public Project(
            @NotNull final String name,
            @Nullable final String description,
            @NotNull final User user
    ) {
        this.name = name;
        this.description = description;
        this.user = user;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o instanceof Project && super.equals(o)) {
            @NotNull final Project project = (Project) o;
            return Objects.equals(name, project.name)
                    && Objects.equals(description, project.description)
                    && Objects.equals(user.id, project.user.id);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, user.id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}