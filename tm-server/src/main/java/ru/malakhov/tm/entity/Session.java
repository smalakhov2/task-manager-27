package ru.malakhov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_session")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Session extends AbstractEntity implements Cloneable {

    public static final long serialVersionUID = 1L;

    @NotNull
    @Column(nullable = false)
    private Long timestamp;

    @NotNull
    @ManyToOne
    private User user;

    @Nullable
    @Column(nullable = false)
    private String signature;

    public Session(
            @NotNull final Long timestamp,
            @NotNull final User user,
            @Nullable final String signature
    ) {
        this.timestamp = timestamp;
        this.user = user;
        this.signature = signature;
    }

    @Nullable
    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o instanceof Session && super.equals(o)) {
            @NotNull final Session session = (Session) o;
            return Objects.equals(timestamp, session.timestamp)
                    && Objects.equals(user.id, session.user.id)
                    && Objects.equals(signature, session.signature);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, timestamp, user.id, signature);
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

}