package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<SessionDto> {

    @NotNull
    List<SessionDto> findAllDto();

    @NotNull
    List<Session> findAllEntity();

    @NotNull
    List<SessionDto> findAllDtoByUserId(@NotNull String userId);

    @NotNull
    List<Session> findAllEntityByUserId(@NotNull String userId);

    @Nullable
    SessionDto findOneDtoById(@NotNull String id);

    @Nullable
    Session findOneEntityById(@NotNull String id);

    void removeAll();

    void removeAllByUserId(@NotNull String userId);

    void removeOneById(@NotNull String id);

}