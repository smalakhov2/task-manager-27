package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.AbstractEntityDto;

import java.util.Collection;

public interface IRepository<E extends AbstractEntityDto> {

    void begin();

    void commit();

    void rollback();

    void close();

    void clear();

    void persist(@NotNull E entity);

    void persist(@NotNull Collection<E> entities);

    void persist(@Nullable E... entities);

    void merge(@NotNull E entity);

    void merge(@NotNull Collection<E> entities);

    void merge(@Nullable E... entities);

    void removeOne(@NotNull E entity);

}