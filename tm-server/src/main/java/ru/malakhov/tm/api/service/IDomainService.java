package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.Domain;
import ru.malakhov.tm.exception.empty.EmptyDomainException;

public interface IDomainService {

    void loadData(@Nullable Domain domain) throws EmptyDomainException;

    void saveData(@Nullable Domain domain) throws EmptyDomainException;

}