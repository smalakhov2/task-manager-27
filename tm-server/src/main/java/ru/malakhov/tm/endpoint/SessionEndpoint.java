package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.endpoint.ISessionEndpoint;
import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.dto.response.Fail;
import ru.malakhov.tm.dto.response.Result;
import ru.malakhov.tm.dto.response.Success;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.user.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint() {
        super(null);
    }

    public SessionEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public Result clearAllSession(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) {
        try {
            serviceLocator.getSessionService().validate(session, Role.ADMIN);
            serviceLocator.getSessionService().removeAll();
            return new Success();
        } catch (@NotNull final AbstractException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public List<SessionDto> getAllSessionList(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getSessionService().findAllDto();
    }

    @Nullable
    @Override
    @WebMethod
    public SessionDto openSession(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ) throws AbstractException {
        return serviceLocator.getSessionService().open(login, password);
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeSession(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) {
        try {
            serviceLocator.getSessionService().close(session);
            return new Success();
        } catch (final AccessDeniedException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeSessionAll(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) {
        try {
            serviceLocator.getSessionService().closeAll(session);
            return new Success();
        } catch (final AccessDeniedException e) {
            return new Fail(e);
        }
    }

    @NotNull
    @WebMethod
    public List<SessionDto> getListSession(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        return serviceLocator.getSessionService().getListSession(session);
    }

    @Nullable
    @WebMethod
    public UserDto getUser(
            @WebParam(name = "session") @Nullable final SessionDto session
    ) throws AbstractException {
        return serviceLocator.getSessionService().getUser(session);
    }

}