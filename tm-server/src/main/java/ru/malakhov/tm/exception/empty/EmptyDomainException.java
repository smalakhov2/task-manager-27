package ru.malakhov.tm.exception.empty;

import ru.malakhov.tm.exception.AbstractException;

public final class EmptyDomainException extends AbstractException {

    public EmptyDomainException() {
        super("Error! Domain is empty...");
    }

}
