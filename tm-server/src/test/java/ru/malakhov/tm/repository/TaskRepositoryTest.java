package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.DisableOnDebug;
import org.junit.rules.Timeout;
import ru.malakhov.tm.AbstractDataTest;
import ru.malakhov.tm.api.repository.ITaskRepository;
import ru.malakhov.tm.category.DataCategory;
import ru.malakhov.tm.dto.TaskDto;
import ru.malakhov.tm.entity.Task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(DataCategory.class)
public final class TaskRepositoryTest extends AbstractDataTest {

    @Rule
    public DisableOnDebug debugTime = new DisableOnDebug(Timeout.seconds(30));

    @NotNull
    private final TaskDto taskOne = new TaskDto("Task1", "", userDto.getId());

    @NotNull
    private final TaskDto taskTwo = new TaskDto("Task2", "", userDto.getId());

    @NotNull
    private final TaskDto taskThree = new TaskDto("Task3", "", adminDto.getId());

    @NotNull
    private final TaskDto taskFour = new TaskDto("Task4", "", adminDto.getId());

    @NotNull
    private final TaskDto unknownTask = new TaskDto("Unknown", "", unknownUserDto.getId());

    @NotNull
    private final List<TaskDto> userTasks = new ArrayList<>(Arrays.asList(taskOne, taskTwo));

    @NotNull
    private final List<TaskDto> adminTasks = new ArrayList<>(Arrays.asList(taskThree, taskFour));

    @NotNull
    private ITaskRepository getRepository() {
        return bootstrap.getTaskService().getRepository();
    }
    
    public TaskRepositoryTest() throws Exception {
        super();
    }

    @Before
    public void before() {
        bootstrap.getUserService().persist(userDto, adminDto);
        bootstrap.getTaskService().persist(userTasks);
        bootstrap.getTaskService().persist(adminTasks);
    }

    @After
    public void after() {
        bootstrap.getTaskService().removeAll();
        bootstrap.getUserService().removeOne(userDto);
        bootstrap.getUserService().removeOne(adminDto);
    }

    @Test
    public void testFindAllDto() {
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final List<TaskDto> tasks = repository.findAllDto();
        Assert.assertEquals(4, tasks.size());
    }

    @Test
    public void testFindAllEntity() {
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final List<Task> tasks = repository.findAllEntity();
        Assert.assertEquals(4, tasks.size());
    }

    @Test
    public void testFindAllDtoByUserId() {
        @NotNull final ITaskRepository repository = getRepository();

        @NotNull final List<TaskDto> tasks = repository.findAllDtoByUserId(userDto.getId());
        Assert.assertEquals(2, tasks.size());

        @NotNull final List<TaskDto> emptyList = repository.findAllDtoByUserId(unknownUserDto.getId());
        Assert.assertEquals(0, emptyList.size());
    }

    @Test
    public void testFindAllEntityByUserId() {
        @NotNull final ITaskRepository repository = getRepository();

        @NotNull final List<Task> tasks = repository.findAllEntityByUserId(userDto.getId());
        Assert.assertEquals(2, tasks.size());

        @NotNull final List<Task> emptyList = repository.findAllEntityByUserId(unknownUserDto.getId());
        Assert.assertEquals(0, emptyList.size());
    }

    @Test
    public void testFindOneDtoById() {
        @NotNull final ITaskRepository repository = getRepository();

        @Nullable final TaskDto task = repository.findOneDtoById(taskOne.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task, taskOne);

        @Nullable final TaskDto unknown = repository.findOneDtoById(unknownTask.getId());
        Assert.assertNull(unknown);
    }

    @Test
    public void testFindOneEntityById() {
        @NotNull final ITaskRepository repository = getRepository();

        @Nullable final Task task = repository.findOneEntityById(taskOne.getId());
        Assert.assertNotNull(task);

        @Nullable final Task unknown = repository.findOneEntityById(unknownTask.getId());
        Assert.assertNull(unknown);
    }

    @Test
    public void testFindOneDtoByIdWithUserId() {
        @NotNull final ITaskRepository repository = getRepository();

        @Nullable final TaskDto task = repository.findOneDtoById(userDto.getId(), taskOne.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task, taskOne);

        @Nullable TaskDto unknown = repository.findOneDtoById(unknownUserDto.getId(), unknownTask.getId());
        Assert.assertNull(unknown);
        
        unknown = repository.findOneDtoById(unknownUserDto.getId(), taskOne.getId());
        Assert.assertNull(unknown);
        
        unknown = repository.findOneDtoById(userDto.getId(), unknownTask.getId());
        Assert.assertNull(unknown);
    }

    @Test
    public void testFindOneEntityByIdWithUserId() {
        @NotNull final ITaskRepository repository = getRepository();

        @Nullable final Task task = repository.findOneEntityById(userDto.getId(), taskOne.getId());
        Assert.assertNotNull(task);

        @Nullable Task unknown = repository.findOneEntityById(unknownUserDto.getId(), unknownTask.getId());
        Assert.assertNull(unknown);
        
        unknown = repository.findOneEntityById(unknownUserDto.getId(), taskOne.getId());
        Assert.assertNull(unknown);
        
        unknown = repository.findOneEntityById(userDto.getId(), unknownTask.getId());
        Assert.assertNull(unknown);
    }

    @Test
    public void testFindOneDtoByIndex() {
        @NotNull final ITaskRepository repository = getRepository();

        @Nullable final TaskDto task = repository.findOneDtoByIndex(userDto.getId(), 0);
        Assert.assertNotNull(task);
        Assert.assertEquals(task, taskOne);

        @Nullable TaskDto unknown = repository.findOneDtoByIndex(unknownUserDto.getId(), 6);
        Assert.assertNull(unknown);
        
        unknown = repository.findOneDtoByIndex(unknownUserDto.getId(), 0);
        Assert.assertNull(unknown);
        
        unknown = repository.findOneDtoByIndex(userDto.getId(), 6);
        Assert.assertNull(unknown);
    }

    @Test
    public void testFindOneEntityByIndex() {
        @NotNull final ITaskRepository repository = getRepository();

        @Nullable final Task task = repository.findOneEntityByIndex(userDto.getId(), 0);
        Assert.assertNotNull(task);

        @Nullable Task unknown = repository.findOneEntityByIndex(unknownUserDto.getId(), 6);
        Assert.assertNull(unknown);
        
        unknown = repository.findOneEntityByIndex(unknownUserDto.getId(), 0);
        Assert.assertNull(unknown);
        
        unknown = repository.findOneEntityByIndex(userDto.getId(), 6);
        Assert.assertNull(unknown);
    }

    @Test
    public void testFindOneDtoByName() {
        @NotNull final ITaskRepository repository = getRepository();

        @Nullable final TaskDto task = repository.findOneDtoByName(userDto.getId(), taskOne.getName());
        Assert.assertNotNull(task);
        Assert.assertEquals(task, taskOne);

        @Nullable TaskDto unknown = repository.findOneDtoByName(unknownUserDto.getId(), unknownTask.getName());
        Assert.assertNull(unknown);
        
        unknown = repository.findOneDtoByName(unknownUserDto.getId(), taskOne.getName());
        Assert.assertNull(unknown);
        
        unknown = repository.findOneDtoByName(userDto.getId(), unknownTask.getName());
        Assert.assertNull(unknown);
    }

    @Test
    public void testFindOneEntityByName() {
        @NotNull final ITaskRepository repository = getRepository();

        @Nullable final Task task = repository.findOneEntityByName(userDto.getId(), taskOne.getName());
        Assert.assertNotNull(task);

        @Nullable Task unknown = repository.findOneEntityByName(unknownUserDto.getId(), unknownTask.getName());
        Assert.assertNull(unknown);
        
        unknown = repository.findOneEntityByName(unknownUserDto.getId(), taskOne.getName());
        Assert.assertNull(unknown);
        
        unknown = repository.findOneEntityByName(userDto.getId(), unknownTask.getName());
        Assert.assertNull(unknown);
    }

    @Test
    public void testRemoveAll() {
        @NotNull final ITaskRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeAll();
            repository.commit();
            @NotNull final List<TaskDto> tasks = repository.findAllDto();
            Assert.assertEquals(0, tasks.size());
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Test
    public void testRemoveAllByUserId() {
        @NotNull final ITaskRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeAllByUserId(unknownUserDto.getId());
            repository.commit();
            @NotNull List<TaskDto> tasks = repository.findAllDto();
            Assert.assertEquals(4, tasks.size());

            repository.begin();
            repository.removeAllByUserId(userDto.getId());
            repository.commit();
            tasks = repository.findAllDto();
            Assert.assertEquals(2, tasks.size());
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Test
    public void testRemoveOneById() {
        @NotNull final ITaskRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeOneById(unknownTask.getId());
            repository.commit();
            @NotNull List<TaskDto> tasks = repository.findAllDto();
            Assert.assertEquals(4, tasks.size());

            repository.begin();
            repository.removeOneById(taskOne.getId());
            repository.commit();
            @Nullable final TaskDto task = repository.findOneDtoById(taskOne.getId());
            Assert.assertNull(task);
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Test
    public void testRemoveOneByIdWithUserId() {
        @NotNull final ITaskRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeOneById(unknownUserDto.getId(), unknownTask.getId());
            repository.commit();
            @NotNull List<TaskDto> tasks = repository.findAllDto();
            Assert.assertEquals(4, tasks.size());

            repository.begin();
            repository.removeOneById(userDto.getId(), unknownTask.getId());
            repository.commit();
            tasks = repository.findAllDto();
            Assert.assertEquals(4, tasks.size());

            repository.begin();
            repository.removeOneById(unknownUserDto.getId(), taskOne.getId());
            repository.commit();
            tasks = repository.findAllDto();
            Assert.assertEquals(4, tasks.size());

            repository.begin();
            repository.removeOneById(userDto.getId(), taskOne.getId());
            repository.commit();
            @Nullable final TaskDto task = repository.findOneDtoById(taskOne.getId());
            Assert.assertNull(task);
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Test
    public void testRemoveOneByIndex() {
        @NotNull final ITaskRepository repository = getRepository();

        try {
            repository.begin();
            repository.removeOneByIndex(unknownUserDto.getId(), 6);
            repository.commit();
            @NotNull List<TaskDto> tasks = repository.findAllDto();
            Assert.assertEquals(4, tasks.size());

            repository.begin();
            repository.removeOneByIndex(userDto.getId(), 6);
            repository.commit();
            tasks = repository.findAllDto();
            Assert.assertEquals(4, tasks.size());

            repository.begin();
            repository.removeOneByIndex(unknownUserDto.getId(), 0);
            repository.commit();
            tasks = repository.findAllDto();
            Assert.assertEquals(4, tasks.size());

            repository.begin();
            repository.removeOneByIndex(userDto.getId(), 0);
            repository.commit();
            @Nullable final TaskDto task = repository.findOneDtoById(taskOne.getId());
            Assert.assertNull(task);
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Test
    public void testRemoveOneByName() {
        @NotNull final ITaskRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeOneByName(unknownUserDto.getId(), unknownTask.getName());
            repository.commit();
            @NotNull List<TaskDto> tasks = repository.findAllDto();
            Assert.assertEquals(4, tasks.size());

            repository.begin();
            repository.removeOneByName(userDto.getId(), unknownTask.getName());
            repository.commit();
            tasks = repository.findAllDto();
            Assert.assertEquals(4, tasks.size());

            repository.begin();
            repository.removeOneByName(unknownUserDto.getId(), taskOne.getName());
            repository.commit();
            tasks = repository.findAllDto();
            Assert.assertEquals(4, tasks.size());

            repository.begin();
            repository.removeOneByName(userDto.getId(), taskOne.getName());
            repository.commit();
            @Nullable final TaskDto task = repository.findOneDtoById(taskOne.getId());
            Assert.assertNull(task);
        } catch (Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

}
