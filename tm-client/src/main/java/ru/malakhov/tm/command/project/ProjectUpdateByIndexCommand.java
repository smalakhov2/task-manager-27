package ru.malakhov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.Result;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-update-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by index.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PROJECT]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.print("ENTER NEW NAME: ");
        @NotNull final String newName = TerminalUtil.nextLine();
        System.out.print("ENTER NEW DESCRIPTION: ");
        @NotNull final String newDescription = TerminalUtil.nextLine();
        @Nullable final SessionDto session = serviceLocator.getPropertyService().getSession();
        @NotNull final Result result = serviceLocator.getProjectEndpoint().updateProjectByIndex(
                session,
                index,
                newName,
                newDescription
        );
        if (result.isSuccess()) System.out.println("[OK]");
        else {
            System.out.println("MESSAGE" + result.getMessage());
            System.out.println("[FAIL]");
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}