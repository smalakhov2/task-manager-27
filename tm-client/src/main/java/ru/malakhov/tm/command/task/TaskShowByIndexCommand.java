package ru.malakhov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.endpoint.TaskDto;
import ru.malakhov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskShowCommand {

    @NotNull
    @Override
    public String name() {
        return "task-show-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by index.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final SessionDto session = serviceLocator.getPropertyService().getSession();
        @Nullable final TaskDto task = serviceLocator.getTaskEndpoint().getTaskByIndex(session, index);
        if (task == null) System.out.println("[FAIL]");
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public boolean secure() {
        return true;
    }

}