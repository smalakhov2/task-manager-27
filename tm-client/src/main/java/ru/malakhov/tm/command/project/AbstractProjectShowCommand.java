package ru.malakhov.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.ProjectDto;

public abstract class AbstractProjectShowCommand extends AbstractCommand {

    protected void showProject(@Nullable final ProjectDto project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

}