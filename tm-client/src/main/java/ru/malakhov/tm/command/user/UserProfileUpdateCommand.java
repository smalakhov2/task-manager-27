package ru.malakhov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.endpoint.AbstractException_Exception;
import ru.malakhov.tm.endpoint.Result;
import ru.malakhov.tm.endpoint.SessionDto;
import ru.malakhov.tm.util.TerminalUtil;

public final class UserProfileUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "update-profile";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update profile.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        System.out.println("[UPDATE-PROFILE]");
        System.out.print("ENTER NEW EMAIL: ");
        @NotNull final String email = TerminalUtil.nextLine();
        System.out.print("ENTER NEW FIRST NAME: ");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.print("ENTER NEW LAST NAME: ");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.print("ENTER NEW MIDDLE NAME: ");
        @NotNull final String middleName = TerminalUtil.nextLine();
        @Nullable final SessionDto session = serviceLocator.getPropertyService().getSession();
        @NotNull final Result result = serviceLocator.getUserEndpoint().updateUser(
                session,
                email,
                firstName,
                lastName,
                middleName
        );
        if (result.isSuccess()) System.out.println("[OK]");
        else {
            System.out.println("MESSAGE" + result.getMessage());
            System.out.println("[FAIL]");
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}