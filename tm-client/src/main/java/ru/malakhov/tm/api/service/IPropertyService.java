package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.endpoint.SessionDto;

public interface IPropertyService {

    @Nullable
    SessionDto getSession();

    void setSession(@Nullable SessionDto session);

    boolean isAuth();

}