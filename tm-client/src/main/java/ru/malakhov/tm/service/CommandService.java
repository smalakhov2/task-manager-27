package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.ICommandRepository;
import ru.malakhov.tm.api.service.ICommandService;
import ru.malakhov.tm.command.AbstractCommand;

import java.util.Map;
import java.util.Set;

public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandReposytory;

    public CommandService(@NotNull final ICommandRepository commandReposytory) {
        this.commandReposytory = commandReposytory;
    }

    @NotNull
    @Override
    public Map<String, AbstractCommand> getCommands() {
        return commandReposytory.getCommands();
    }

    @NotNull
    @Override
    public Set<String> getCommandsName() {
        return commandReposytory.getCommandsName();
    }

    @NotNull
    @Override
    public Set<String> getCommandsArg() {
        return commandReposytory.getCommandsArg();
    }

    @Override
    public void add(@Nullable final AbstractCommand command) {
        if (command == null) return;
        commandReposytory.add(command.name(), command);
    }

}