package ru.malakhov.tm.constant;

import org.jetbrains.annotations.NotNull;

public interface InformationConst {

    @NotNull
    String VERSION = "1.2.6";

}
